<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230522095606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('ALTER TABLE exercise_table DROP FOREIGN KEY FK_D31F7884BEFD98D1');
        $this->addSql('ALTER TABLE exercise_table DROP FOREIGN KEY FK_D31F7884E934951A');
        $this->addSql('DROP INDEX IDX_D31F7884E934951A ON exercise_table');
        $this->addSql('ALTER TABLE exercise_table ADD id INT AUTO_INCREMENT NOT NULL, DROP training_id, DROP exercise_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE exercise_table MODIFY id INT NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON exercise_table');
        $this->addSql('ALTER TABLE exercise_table ADD training_id INT NOT NULL, ADD exercise_id INT DEFAULT NULL, DROP id');
        $this->addSql('ALTER TABLE exercise_table ADD CONSTRAINT FK_D31F7884BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE exercise_table ADD CONSTRAINT FK_D31F7884E934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_D31F7884E934951A ON exercise_table (exercise_id)');
        $this->addSql('ALTER TABLE exercise_table ADD PRIMARY KEY (training_id)');
    }
}
