<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230522094858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('ALTER TABLE exercise_table ADD id INT AUTO_INCREMENT NOT NULL, CHANGE training_id training_id INT DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('CREATE INDEX IDX_D31F7884BEFD98D1 ON exercise_table (training_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE exercise_table MODIFY id INT NOT NULL');
        $this->addSql('DROP INDEX IDX_D31F7884BEFD98D1 ON exercise_table');
        $this->addSql('DROP INDEX `PRIMARY` ON exercise_table');
        $this->addSql('ALTER TABLE exercise_table DROP id, CHANGE training_id training_id INT NOT NULL');
        $this->addSql('ALTER TABLE exercise_table ADD PRIMARY KEY (training_id)');
    }
}
