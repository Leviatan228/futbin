<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230508141657 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exercise_training (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('ALTER TABLE training ADD id_exercise INT NOT NULL, CHANGE n_exercises id_user INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE exercise_training');
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE training ADD n_exercises INT NOT NULL, DROP id_user, DROP id_exercise');
    }
}
