<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230518103126 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('ALTER TABLE training ADD exe_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE training ADD CONSTRAINT FK_D5128A8F1278EA4A FOREIGN KEY (exe_id_id) REFERENCES exercise (id)');
        $this->addSql('CREATE INDEX IDX_D5128A8F1278EA4A ON training (exe_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE training DROP FOREIGN KEY FK_D5128A8F1278EA4A');
        $this->addSql('DROP INDEX IDX_D5128A8F1278EA4A ON training');
        $this->addSql('ALTER TABLE training DROP exe_id_id');
    }
}
