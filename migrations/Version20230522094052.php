<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230522094052 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exercise_table (training_id INT NOT NULL, exercise_id INT DEFAULT NULL, INDEX IDX_D31F7884E934951A (exercise_id), PRIMARY KEY(training_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exercise_table ADD CONSTRAINT FK_D31F7884E934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id)');
        $this->addSql('ALTER TABLE exercise_table ADD CONSTRAINT FK_D31F7884BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id)');
        $this->addSql('ALTER TABLE `table` DROP FOREIGN KEY FK_F6298F461278EA4A');
        $this->addSql('ALTER TABLE `table` DROP FOREIGN KEY FK_F6298F46CC4C84A3');
        $this->addSql('DROP TABLE `table`');
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `table` (tra_id_id INT NOT NULL, exe_id_id INT DEFAULT NULL, INDEX IDX_F6298F461278EA4A (exe_id_id), PRIMARY KEY(tra_id_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE `table` ADD CONSTRAINT FK_F6298F461278EA4A FOREIGN KEY (exe_id_id) REFERENCES exercise (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE `table` ADD CONSTRAINT FK_F6298F46CC4C84A3 FOREIGN KEY (tra_id_id) REFERENCES training (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE exercise_table DROP FOREIGN KEY FK_D31F7884E934951A');
        $this->addSql('ALTER TABLE exercise_table DROP FOREIGN KEY FK_D31F7884BEFD98D1');
        $this->addSql('DROP TABLE exercise_table');
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
    }
}
