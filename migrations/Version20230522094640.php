<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230522094640 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('DROP INDEX `primary` ON exercise_table');
        $this->addSql('ALTER TABLE exercise_table CHANGE exercise_id exercise_id INT NOT NULL');
        $this->addSql('CREATE INDEX IDX_D31F7884BEFD98D1 ON exercise_table (training_id)');
        $this->addSql('ALTER TABLE exercise_table ADD PRIMARY KEY (exercise_id, training_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('DROP INDEX IDX_D31F7884BEFD98D1 ON exercise_table');
        $this->addSql('DROP INDEX `PRIMARY` ON exercise_table');
        $this->addSql('ALTER TABLE exercise_table CHANGE exercise_id exercise_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise_table ADD PRIMARY KEY (training_id)');
    }
}
