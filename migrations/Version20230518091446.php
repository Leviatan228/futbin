<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230518091446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description VARCHAR(20000) NOT NULL');
        $this->addSql('DROP INDEX IDX_D37E2A67BEFD98D1 ON exercise_training');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercise CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('CREATE INDEX IDX_D37E2A67BEFD98D1 ON exercise_training (training_id)');
    }
}
