var i=1;
function fetchAndPrintExercises() {
    // Hacer la solicitud GET a la API para obtener los datos de los ejercicios
    fetch('/api/exercise/')
        .then(response => response.json())
        .then(exercises => {
            // Obtener el contenedor en la plantilla HTML donde se mostrarán los datos
            const container = document.getElementById('exercise-container');

            // Iterar sobre los datos de los ejercicios y construir el HTML correspondiente
            exercises.forEach(exercise => {

                const exerciseElement = document.createElement('div'+i);
                i=i+1;
                        exerciseElement.innerHTML = `
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <div style="visibility: hidden">${exercise.id}</div>
                                        <h3>${exercise.Name}</h3>
                                        <p>Difficulty: ${exercise.Difficulty}</p>
                                    </div>
                                   <div class="flip-card-back">
                                        <p>${exercise.Description}</p> 
                                   </div>
                               </div>
                           </div>
                           <a onclick="select(${exercise.id})">Seleccionar calentamiento</a>
                           
        `;
                // Agregar el elemento del ejercicio al contenedor en la plantilla HTML
                container.appendChild(exerciseElement);
            });
        })
        .catch(error => console.error(error));
}