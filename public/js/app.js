(function($){
    // Obtener el botón por su ID
    var guardarBtn = document.getElementById('guardarBtn');

    // Agregar un event listener al botón
    guardarBtn.addEventListener('click', function() {
        // Crear un arreglo para almacenar los IDs
        var ids = [];

        // Iterar sobre las claves almacenadas en el localStorage
        for (var i = 1; i <= amount; i++) {
            var clave = "selection"+i;

            // Verificar si la clave existe en el localStorage
            if (localStorage.getItem(clave)) {
                // Obtener el valor correspondiente a la clave y agregarlo al arreglo de IDs
                var id = localStorage.getItem(clave);
                ids.push(id);
            }else{console.log('Este no existe');}
        }

        // Enviar los IDs al servidor
        $.ajax({
            url: '/selection/save', // La URL del archivo PHP para guardar los datos en MySQL
            type: 'POST',
            data: { ids: ids }, // Pasar los IDs como datos en la solicitud POST
            success: function(response) {
                // Manejar la respuesta del servidor si es necesario
                console.log(response);
            },
            error: function(xhr, status, error) {
                // Manejar errores en la solicitud AJAX si es necesario
                console.log(error);
            }
        });
    });
})(jQuery);