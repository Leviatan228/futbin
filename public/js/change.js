var amount = window.amount;
window.addEventListener("load", (event) => {
    for (let i = 1; i <=amount; i++) {
        const savedSessionExercise = localStorage.getItem('selection'+i);
        let cardExercise = document.getElementById('card-'+i);
        console.log(savedSessionExercise);
        if (savedSessionExercise) {
            const xget = new XMLHttpRequest();
            const getExercisePath = "/api/exercise/"+savedSessionExercise;
            xget.open('GET', getExercisePath, true);
            xget.send();
            xget.onload = function() {
                if (xget.status === 200) {

                    console.log(cardExercise);
                    const exercise = JSON.parse(xget.responseText);
                    console.log(exercise);
                    if (exercise) {
                        cardExercise.querySelector('.id').innerHTML = exercise.id;
                        cardExercise.querySelector('.name').innerHTML = exercise.Name;
                        cardExercise.querySelector('.difficulty').innerHTML = exercise.Difficulty;
                        document.getElementById('step' + i).innerHTML = xget.responseText;
                    } else {
                        console.log('Respuesta inválida');
                    }
                } else {
                    console.log('Error en la solicitud. Código de estado: ' + xget.status);
                }
            };

            xget.onerror = function() {
                console.log('Error en la solicitud');
            };
        } else {
            console.log('No existe tal cosa');
        }
    }
});


















