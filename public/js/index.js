function select(id) {
    const urlParams = new URLSearchParams(window.location.search);
    localStorage.setItem('selection' + urlParams.get('step'), id);
    window.location.href = '/selection';
}

function clean() {
    const urlParams = new URLSearchParams(window.location.search);
    localStorage.removeItem('selection' + urlParams.get('step'));
    window.location.href = 'selection';
}
function redireccionarAInformacion(id) {
    window.location.href = '/exercise-list?step=' + id;
}
