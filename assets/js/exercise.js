fetch('/api/exercise/')
    .then(response => response.json())
    .then(data => {
        // Llamar a la función que imprime los datos en la plantilla HTML.twig
        printData(data);
    })
    .catch(error => console.error(error));

// Función para imprimir los datos en la plantilla HTML.twig
function printData(data) {
    // Obtener el contenedor en la plantilla HTML donde se mostrarán los datos
    const container = document.getElementById('exercise-container');

    // Recorrer los datos y construir el HTML correspondiente
    data.forEach(exercise => {
        const exerciseElement = document.createElement('div');
        exerciseElement.innerHTML = `
      <h3>${exercise.title}</h3>
      // <img src="${exercise.img}" alt="Exercise Image">
      <p>Difficulty: ${exercise.dif}</p>
    `;

        // Agregar el elemento del ejercicio al contenedor en la plantilla HTML
        container.appendChild(exerciseElement);
    });
}