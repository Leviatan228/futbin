new Vue({
    el: '#app',
    data: {
        exercises: [],
    },
    mounted() {
        this.fetchExercises();
    },
    methods: {
        fetchExercises() {
            fetch('/api/exercise')
                .then(response => response.json())
                .then(data => {
                    this.exercises = data['hydra:member'];
                })
                .catch(error => {
                    console.error(error);
                });
        },
    },
});