<?php

namespace App\Repository;

use App\Entity\Exercise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Exercise>
 *
 * @method Exercise|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exercise|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exercise[]    findAll()
 * @method Exercise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExerciseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exercise::class);
    }

    public function save(Exercise $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Exercise $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Exercise[] Returns an array of Exercise objects
     */
    public function FindExercise($value): array
    {
        return $this->createQueryBuilder('exercise')
            ->andWhere('exercise.Position = :val')
            ->setParameter('val', $value)
            ->orderBy('exercise.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllExercises()
    {
        return $this->createQueryBuilder('exercise')
            ->getQuery()
            ->getResult();
    }

    public function findExerciseById($id)
    {
        return $this->createQueryBuilder('exercise')
            ->andWhere('exercise.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneTraining($idTraining)
    {
    return $this->createQueryBuilder('e')
        ->select('e.Name, e.Difficulty')
        ->join('App\Entity\ExerciseTable', 'et', 'WITH', 'et.exercise = e.id')
        ->andWhere('et.training = :idTraining')
        ->setParameter('idTraining', $idTraining)
        ->getQuery()
        ->getResult();
}

}
