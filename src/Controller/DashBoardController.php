<?php

namespace App\Controller;

use App\Entity\Training;
use App\Form\TrainingType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use DateTimeImmutable;


class DashBoardController extends AbstractController
{
    #[Route(path: '/', name: 'dashboard')]
    public function index(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $user = $this->getUser(); //OBTENGO AL USUARIO ACTUALMENTE LOGUEADO
        if ($user)
        {
            $training = new Training();
            $form = $this->createForm(TrainingType::class, $training);
            $trainings = $em->getRepository(Training::class)->findBy(['user'=>$user]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $user = $this->getUser();
                $training->setUser($user);
                date_default_timezone_set('Europe/Madrid');
                $fecha_date= new DateTimeImmutable();
                $training->setDate( $fecha_date);
                $em->persist($training);
                $em->flush();
                return $this->redirectToRoute('app_selection');
            }


            $query = $em->getRepository(Training::class)->findBy(['user'=>$user]);

            $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                2 /*limit per page*/
            );


            return $this->render('dash_board/index.html.twig', [
                'userId'=>$user,
                'form' => $form->createView(),
                'trainings'=>$pagination


            ]);
        }
        else{
        return $this->redirectToRoute('app_login');
        }
    }
}



