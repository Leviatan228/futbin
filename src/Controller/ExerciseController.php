<?php

namespace App\Controller;

use App\Entity\Exercise;
use App\Repository\ExerciseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercises-list')]
class ExerciseController extends AbstractController
{
    private $exerciseRepository;

    public function __construct(ExerciseRepository $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }
    #[Route('/', name: 'app_exercise')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {
        $exercises = $this->exerciseRepository->findAll();

        return $this->render('exercise/API.html.twig', [
            'exercises' => $exercises,
        ]);
    }
}