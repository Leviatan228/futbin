<?php

namespace App\Controller;


use App\Entity\ExerciseTable;
use App\Entity\Training;
use App\Repository\ExerciseRepository;
use App\Repository\TrainingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class SelectionController extends AbstractController
{
    #[Route('/selection', name: 'app_selection')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {
        $user = $this->getUser();

        $query2 = $em->getRepository(Training::class)->FindAmount($user);
        $result = reset($query2);
        $amount = intval($result['amount']);

        return $this->render('selection/index.html.twig', [
            'controller_name' => 'SelectionController',
            'amount' => $amount,

        ]);
    }

    #[Route('/selection/save', name: 'app_save')]
    public function guardarEnMySQL(TrainingRepository $trainingrepository, ExerciseRepository $exerciseRepository, EntityManagerInterface $em, Request $request): Response
    {
        $user = $this->getUser();

        $ids = $request->request->get('ids');
        $entityManager = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Training::class)->FindTraining($user);
        $training=$trainingrepository->find($query);


            // Resto del código...
            foreach ($ids as $id_e) {
                $exercise = $exerciseRepository->find($id_e);
                $table = new ExerciseTable();
                $table->setTraining($training);
                $table->setExercise($exercise);
                $entityManager->persist($table);

            }
            $entityManager->flush();
            return $this->redirectToRoute('dashboard');
    }
    #[Route('/selection/{id}', name: 'app_mytraining')]
    public function mypost($id,ExerciseRepository $exerciseRepository): Response
    {
        $idTraining = $id; // Cambia esto por el id_training deseado
        $ejercicios = $exerciseRepository->findOneTraining($idTraining);

        return $this->render('/selection/MyTraining.html.twig', [
            'ejercicios' => $ejercicios,
        ]);
    }
}

