<?php

namespace App\Controller\API;

use App\Entity\Exercise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/exercise')]
class ExerciseResource extends AbstractController
{
    #[Route('/', name: 'api_exercises_list', methods: ['GET'])]
    public function list()
    {
        $exercises = $this->getDoctrine()->getRepository(Exercise::class)->findAllExercises();

        // Puedes personalizar la respuesta según tus necesidades
        return $this->json($exercises);
    }

    #[Route('/{id}', name: 'api_exercises_get', methods: ['GET'])]
    public function getId($id)
    {
        $exercise = $this->getDoctrine()->getRepository(Exercise::class)->findExerciseById($id);

        if (!$exercise) {
            throw $this->createNotFoundException('Exercise not found');
        }

        // Puedes personalizar la respuesta según tus necesidades
        return $this->json($exercise);
    }

    #[Route('/', name: 'api_exercise_post',methods: 'POST')]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $exercise = new Exercise();
        $exercise->setName($data['name']);
        $exercise->setDescription($data['description']);
        $exercise->setImage($data['image']);
        $exercise->setDifficulty($data['difficulty']);
        $exercise->setPosition($data['position']);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($exercise);
        $entityManager->flush();

        return new JsonResponse($exercise->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'api_exercise_update',methods: 'PUT')]
    public function update(Request $request, Exercise $exercise): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $exercise->setName($data['name']);
        $exercise->setDescription($data['description']);
        $exercise->setImage($data['image']);
        $exercise->setDifficulty($data['difficulty']);
        $exercise->setPosition($data['position']);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new JsonResponse($exercise->toArray());
    }

    #[Route('/{id}', name: 'api_exercise_delete',methods: 'DELETE')]
    public function delete(Exercise $exercise): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($exercise);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

}