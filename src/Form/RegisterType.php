<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email',EmailType::class)
            ->add('password',PasswordType::class)
            ->add('name')
            ->add('age', BirthdayType::class)
            ->add('position', ChoiceType::class, [
                'choices'  => [
                    'DEFENSA'=>[
                    'POR' => "POR",
                    'DFC' => "DFC",
                    'LI' => "LI",
                    'LD' => "LD"
                    ],'MEDIOCENTRO'=>[
                    'MC' => "MC",
                    'MCD' => "MCD",
                    'MCO' => "MCO",
                    'LM' => "LM",
                    'RM' => "RM"
                    ],'DELANTERO'=>[
                    'DC' => "DC",
                    'LW' => "LW",
                    'RW' => "RW"]
                ],
            ])
            ->add('height', IntegerType::class)
            ->add('weight', IntegerType::class)
            ->add('register',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
